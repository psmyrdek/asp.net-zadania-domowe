﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;
using WebApplication1.ViewModels;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            PeopleViewModel model = new PeopleViewModel
            {
                People = new List<Person>
                {
                    new Person { Age = 21, FirstName = "John", LastName = "Doe"},
                    new Person { Age = 15, FirstName = "Tom", LastName = "Doe"},
                    new Person { Age = 24, FirstName = "Mark", LastName = "Doe"},
                    new Person { Age = 32, FirstName = "Frank", LastName = "Doe"},
                    new Person { Age = 16, FirstName = "Doe", LastName = "Doe"}
                }
            };
            return View(model);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}